# Решение уровня Light [Проекта](https://gitlab.com/va1erify-deusops/task-26-01-2024/about_project)

## Структура репозитория

```
.
├── .gitlab-ci.yml
├── Base.gitlab-ci.yml                        # Вспомогательный CI для хранения состояния в Gitlab Terraform states
├── infrastructure                            # Директория с Terraform файламы
│   ├── backend.tf
│   ├── cloud-config
│   ├── main.tf
│   ├── outputs.tf
│   ├── provider.tf
│   └── variables.tf
├── README.md
└── scripts                                    # Скрипты для настройки виртаульной машины
    ├── 2048.service
    ├── app_build.sh
    ├── app_download.sh
    ├── app_install_dependencies.sh
    ├── app_run.sh
    ├── enable_service.sh
    ├── nginx_configure_proxy.sh
    ├── nginx_delete_default_config_link.sh
    ├── nginx_install.sh
    ├── node16_install.sh
    └── start_all.sh                            # Скрипт, запускающий все остальные в определенной последовательности
```

## Основные компоненты работы CI/CD

1. **Структура пайплайна**:
    - Пайплайн состоит из нескольких этапов, таких
      как `validate`, `build`, `deploy-infrastructure`, `deploy-app`, `cleanup`, каждый из которых выполняет
      определенные действия по развертыванию, настройке и очистке инфраструктуры.

2. **Использование Terraform**:
    - Инфраструктура разворачивается с использованием Terraform, что обеспечивает управляемость, масштабируемость и
      воспроизводимость окружений.

3. **Автоматизация сборки и развертывания**:
    - Сборка и развертывание инфраструктуры автоматизированы с помощью GitLab CI/CD, что позволяет ускорить процесс
      разработки и уменьшить вероятность ошибок.

4. **Управление состоянием Terraform**:
    - Состояние Terraform хранится в GitLab Terraform states, что обеспечивает безопасное и централизованное управление
      состоянием инфраструктуры.

5. **Ручное выполнение задач**:
    - Некоторые этапы, такие как `build`, `deploy-infrastructure`, `deploy-app`, `cleanup`, запускаются вручную из
      веб-интерфейса GitLab.

6. **Использование артефактов**:
    - В процессе развертывания инфраструктуры генерируется JSON-файл с выводами `gitlab-terraform output`, который
      сохраняется как артефакт для передачи данных между этапами.

7. **Настройка и запуск скриптов на удаленной машине**:
    - Для настройки и запуска приложения на виртуальной машине используются скрипты из директории `scripts/`, которые
      копируются и выполняются на удаленной машине с использованием SSH.

## Gitlab CI/CD vars

| Type     | Key             | Description                                                                                                  |
|----------|-----------------|--------------------------------------------------------------------------------------------------------------|
| Variable | CLOUD_ID        | Yandex cloud ID. Used as a variable in the Terraform pipeline to identify the Yandex Cloud ID.               |
| Variable | FOLDER_ID       | Yandex folder ID. Used as a variable in the Terraform pipeline to specify the target folder in Yandex Cloud. |
| Variable | TOKEN           | Masked Yandex token. Used as a variable in the Terraform pipeline to authenticate with the Yandex Cloud API. |
| FILE     | SSH_PRIVATE_KEY | Masked Key for accessing the VM.                                                                             |