#!/bin/bash

# Указываем значения по умолчанию для портов
HTTP_PORT=80
PROXY_PORT=8080

# Проверяем, переданы ли аргументы для переопределения портов
if [ $# -eq 2 ]; then
    HTTP_PORT=$1
    PROXY_PORT=$2
fi

if [ -f /etc/nginx/sites-available/local_proxy ]; then
    echo "Конфигурационный файл для прокси уже существует."
else
    echo "Настройка конфигурационного файла для прокси с портами HTTP: $HTTP_PORT и Proxy: $PROXY_PORT"

    cat <<EOF | sudo tee /etc/nginx/sites-available/local_proxy > /dev/null
server {
    listen $HTTP_PORT;
    server_name localhost;

    location / {
        proxy_pass http://127.0.0.1:$PROXY_PORT;
        proxy_set_header Host \$host;
        proxy_set_header X-Real-IP \$remote_addr;
        proxy_set_header X-Forwarded-For \$proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto \$scheme;
    }
}
EOF

    sudo ln -s /etc/nginx/sites-available/local_proxy /etc/nginx/sites-enabled/

    sudo systemctl restart nginx

    echo "Конфигурационный файл для прокси успешно создан и применен."
fi
