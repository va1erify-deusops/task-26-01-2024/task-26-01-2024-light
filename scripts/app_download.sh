#!/bin/bash

USERNAME="va1erify"
PROJECT_ID="56148157"
PACKAGE_NAME="app-2048-archive"
PACKAGE_VERSION="1.0.0"
FILE_NAME="2048-game-1.0.0.tar.gz"
PATH_TO_NEW_DIRECTORY="./2048-game"
TOKEN=""

if [ $# -eq 7 ]; then
    USERNAME=$1
    PROJECT_ID=$2
    PACKAGE_NAME=$3
    PACKAGE_VERSION=$4
    FILE_NAME=$5
    PATH_TO_NEW_DIRECTORY=$6
    TOKEN=$7
fi

echo "Скачиваем архив"
wget --user "$USERNAME:$TOKEN" \
     "https://gitlab.com/api/v4/projects/$PROJECT_ID/packages/generic/$PACKAGE_NAME/$PACKAGE_VERSION/$FILE_NAME"

echo "Проверям существует ли директория $PATH_TO_NEW_DIRECTORY, если нет, то создаем"
if [ ! -d $PATH_TO_NEW_DIRECTORY ]; then
    mkdir -p $PATH_TO_NEW_DIRECTORY
    echo "Директория не существует, создаем"
fi

echo "Разархивируем"
tar -xvf $FILE_NAME -C $PATH_TO_NEW_DIRECTORY

echo "Удаляем архив"
rm $FILE_NAME

