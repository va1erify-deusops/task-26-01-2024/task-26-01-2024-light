#!/bin/bash

PATH_TO_APP_DIRECTORY="."

if [ $# -eq 1 ]; then
    PATH_TO_APP_DIRECTORY=$1
fi

echo "Директория проекта - $PATH_TO_APP_DIRECTORY"

# Переходим в директорию с приложением
cd $PATH_TO_APP_DIRECTORY

# Запуск приложения (scripts/package.json)
echo "Run project"
npm start