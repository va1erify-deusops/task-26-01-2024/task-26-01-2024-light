#!/bin/bash

if ! command -v nginx &> /dev/null; then
    echo "Nginx не установлен. Начинаем установку..."

    sudo apt update
    sudo apt install nginx -y

    if systemctl status nginx &> /dev/null; then
        echo "Nginx успешно установлен и запущен."
    else
        echo "Произошла ошибка при установке и/или запуске Nginx."
    fi
else
    echo "Nginx уже установлен"
fi

