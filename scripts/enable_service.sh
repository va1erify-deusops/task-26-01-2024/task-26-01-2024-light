#!/bin/bash

SERVICE_NAME="2048.service"  # Имя сервиса
PATH_TO_SERVICE_FILE_IN_ETC="/etc/systemd/system/$SERVICE_NAME"  # Путь к файлу сервиса
PATH_TO_SERVICE_FILE="./2048.service"

if [ $# -eq 3 ]; then
    SERVICE_NAME=$1
    PATH_TO_SERVICE_FILE_IN_ETC=$2
    PATH_TO_SERVICE_FILE=$3
fi


# Проверяем, существует ли файл сервиса
if [ -f "$PATH_TO_SERVICE_FILE_IN_ETC" ]; then
    # Если файл сервиса существует, проверяем, запущен ли сервис
    if systemctl is-active --quiet "$SERVICE_NAME"; then
        echo "Сервис $SERVICE_NAME уже запущен."
        exit 0
    fi
else
    # Если файл сервиса не существует, копируем его
    echo "Сервис $SERVICE_NAME не существует. Создаем и запускаем его."

    # Копируем файл сервиса
    sudo cp $PATH_TO_SERVICE_FILE "$PATH_TO_SERVICE_FILE_IN_ETC"

    # Делаем сервис доступным при загрузке системы
    sudo systemctl enable "$SERVICE_NAME"

    # Запускаем сервис
    sudo systemctl start "$SERVICE_NAME"

    # Проверяем статус сервиса после запуска
    if systemctl is-active --quiet "$SERVICE_NAME"; then
        echo "Сервис $SERVICE_NAME успешно запущен."
    else
        echo "Ошибка при запуске сервиса $SERVICE_NAME."
    fi

    exit 0
fi

echo "Не удалось выполнить запуск сервиса $SERVICE_NAME."
exit 1
