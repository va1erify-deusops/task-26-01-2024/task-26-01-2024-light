#!/bin/bash

# Путь к файлу конфигурации Nginx
NGING_CONFIG_DEFAULT_LINK_PATH="/etc/nginx/sites-enabled/default"

# Проверяем, существует ли файл конфигурации
if [ -e "$NGING_CONFIG_DEFAULT_LINK_PATH" ]; then
    # Проверяем, является ли ссылкой
    if [ -L "$NGING_CONFIG_DEFAULT_LINK_PATH" ]; then
        # Удаляем ссылку
        sudo rm "$NGING_CONFIG_DEFAULT_LINK_PATH"
        echo "Ссылка в файле конфигурации Nginx удалена."
    else
        echo "Файл конфигурации Nginx не является символической ссылкой."
    fi
else
    echo "Файл конфигурации Nginx не существует."
fi