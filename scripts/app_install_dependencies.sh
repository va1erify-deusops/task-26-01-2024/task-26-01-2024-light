#!/bin/bash

PATH_TO_APP_DIRECTORY="."

if [ $# -eq 1 ]; then
    PATH_TO_APP_DIRECTORY=$1
fi

echo "Директория проекта - $PATH_TO_APP_DIRECTORY"

cd $PATH_TO_APP_DIRECTORY

echo "Install dependencies"
npm install --include=dev