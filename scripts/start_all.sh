#!/bin/bash

# Объявляем дефолтные переменные
DEFAULT_VALUES=(
"va1erify" # USERNAME
"56148157" # PROJECT_ID
"app-2048-archive" # PACKAGE_NAME
"1.0.0" # PACKAGE_VERSION
"2048-game-1.0.0.tar.gz" # FILE_NAME
"./2048-game" # PATH_TO_NEW_DIRECTORY
"" # TOKEN
"./2048-game" # PATH_TO_APP_DIRECTORY
"2048.service" # SERVICE_NAME
"/etc/systemd/system/$SERVICE_NAME" # PATH_TO_SERVICE_FILE_IN_ETC
"./2048.service" # PATH_TO_SERVICE_FILE
)

# Объявляем массив для аргументов
ARGS=("$@")  # Записываем все аргументы в массив ARGS


# Проверяем, переданы ли аргументы
if [ ${#ARGS[@]} -ge 11 ]; then
    # Если переданы аргументы, используем их
    USERNAME=${ARGS[0]}
    PROJECT_ID=${ARGS[1]}
    PACKAGE_NAME=${ARGS[2]}
    PACKAGE_VERSION=${ARGS[3]}
    FILE_NAME=${ARGS[4]}
    PATH_TO_NEW_DIRECTORY=${ARGS[5]}
    TOKEN=${ARGS[6]}
    PATH_TO_APP_DIRECTORY=${ARGS[7]}
    SERVICE_NAME=${ARGS[8]}
    PATH_TO_SERVICE_FILE_IN_ETC=${ARGS[9]}
    PATH_TO_SERVICE_FILE=${ARGS[10]}
else
    # Если не переданы аргументы, используем значения по умолчанию
    USERNAME=${DEFAULT_VALUES[0]}
    PROJECT_ID=${DEFAULT_VALUES[1]}
    PACKAGE_NAME=${DEFAULT_VALUES[2]}
    PACKAGE_VERSION=${DEFAULT_VALUES[3]}
    FILE_NAME=${DEFAULT_VALUES[4]}
    PATH_TO_NEW_DIRECTORY=${DEFAULT_VALUES[5]}
    TOKEN=${DEFAULT_VALUES[6]}
    PATH_TO_APP_DIRECTORY=${DEFAULT_VALUES[7]}
    SERVICE_NAME=${DEFAULT_VALUES[8]}
    PATH_TO_SERVICE_FILE_IN_ETC=${DEFAULT_VALUES[9]}
    PATH_TO_SERVICE_FILE=${DEFAULT_VALUES[10]}
fi

echo "Запуск скрипта для установки Node.js версии 16..."
./node16_install.sh
if [ $? -ne 0 ]; then
  echo "Ошибка: Не удалось установить Node.js версии 16. Проверьте скрипт node16_install.sh."
  exit 1
fi

echo "Запуск скрипта для установки Nginx..."
./nginx_install.sh
if [ $? -ne 0 ]; then
  echo "Ошибка: Не удалось установить Nginx. Проверьте скрипт nginx_install.sh."
  exit 1
fi

echo "Запуск скрипта для удаления default link в sites-enabled..."
./nginx_delete_default_config_link.sh
if [ $? -ne 0 ]; then
  echo "Ошибка: Не удалось установить Nginx. Проверьте скрипт nginx_install.sh."
  exit 1
fi

echo "Запуск скрипта для установки конфига proxy на localhost..."
./nginx_configure_proxy.sh
if [ $? -ne 0 ]; then
  echo "Ошибка: Не удалось установить Nginx. Проверьте скрипт nginx_install.sh."
  exit 1
fi

echo "Запуск скрипта для загрузки приложения..."
./app_download.sh $USERNAME $PROJECT_ID $PACKAGE_NAME $PACKAGE_VERSION $FILE_NAME $PATH_TO_NEW_DIRECTORY $TOKEN
if [ $? -ne 0 ]; then
  echo "Ошибка: Не удалось загрузить приложение. Проверьте скрипт app_download.sh."
  exit 1
fi

echo "Запуск скрипта для установки зависимостей приложения..."
./app_install_dependencies.sh $PATH_TO_APP_DIRECTORY
if [ $? -ne 0 ]; then
  echo "Ошибка: Не удалось загрузить приложение. Проверьте скрипт app_install_dependencies.sh."
  exit 1
fi

echo "Запуск скрипта для сборки приложения..."
./app_build.sh $PATH_TO_NEW_DIRECTORY
if [ $? -ne 0 ]; then
  echo "Ошибка: Не удалось собрать приложение. Проверьте скрипт app_build.sh."
  exit 1
fi

echo "Запуск скрипта для создания systemd..."
./enable_service.sh $SERVICE_NAME $PATH_TO_SERVICE_FILE_IN_ETC $PATH_TO_SERVICE_FILE
if [ $? -ne 0 ]; then
  echo "Ошибка: Не удалось Запустить демон. Проверьте скрипт enable_service.sh."
  exit 1
fi

echo "Все скрипты успешно выполнены!"
