#!/bin/bash

node_version=$(node -v)

if [ "$node_version" == "v16.16.0" ]; then
  echo "Node.js версии 16.16.0 уже установлен!"
  exit 0
fi

echo "Загрузка архива Node.js..."
wget https://nodejs.org/dist/v16.16.0/node-v16.16.0-linux-x64.tar.xz

expected_sha="edcb6e9bb049ae365611aa209fc03c4bfc7e0295dbcc5b2f1e710ac70384a8ec"
actual_sha=$(sha256sum node-v16.16.0-linux-x64.tar.xz | awk '{print $1}')

if [ "$expected_sha" != "$actual_sha" ]; then
  echo "Ошибка: SHA256 хэш не соответствует!"
  exit 1
fi

echo "SHA256 хэш совпадает"

echo "Распаковка архива"
tar -xf node-v16.16.0-linux-x64.tar.xz

echo "Перемещаем необходимые файлы в /usr/local"
sudo cp node-v16.16.0-linux-x64/bin/node /usr/local/bin
sudo ln -s "../lib/node_modules/npm/bin/npm-cli.js" /usr/local/bin/npm
sudo cp -r node-v16.16.0-linux-x64/lib/node_modules /usr/local/lib


echo "Удаляем архив"
rm node-v16.16.0-linux-x64.tar.xz

node_version=$(node -v)

echo "Удаляем дистрибутив"
rm -rf node-v16.16.0-linux-x64

if [ "$node_version" != "v16.16.0" ]; then
  echo "Ошибка: Не удалось установить Node.js версии 16.16.0!"
  exit 1
fi

echo "Node.js версии 16.16.0 успешно установлен!"
