output "network_name" {
  value = module.network.network_name
}

output "network_id" {
  value = module.network.network_id
}

output "subnetwork_id" {
  value = module.subnetwork.subnetwork_id
}

output "subnetwork_cidr" {
  value = module.subnetwork.subnetwork_v4_cidr_blocks
}

output "subnetwork_zone" {
  value = module.subnetwork.subnetwork_zone
}

output "vm_hostname" {
  value = module.vm.hostname
}

output "mv_external_ip" {
  value = module.vm.external_ip
}
